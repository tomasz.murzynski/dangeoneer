
import java.util.Random;

public class Kostka {

    int iloscScianKostki;

    public Kostka() {
        this.iloscScianKostki = 6;
    }

    public Kostka(int iloscScianKostki) {
        this.iloscScianKostki = iloscScianKostki;
    }

    public int RzutKostka(){
        Random random = new Random();
        int wynikRzutuKostka = random.nextInt(iloscScianKostki) + 1;
        return  wynikRzutuKostka;
    }
}