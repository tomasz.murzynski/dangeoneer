package com.ecokorporacja.dungeoneer.karty;

import org.junit.jupiter.api.Test;
import static org.junit.Assert.*;

public class BohaterTest {

    Bohater bohater = new Bohater("Test Bohater", 1, 0,2,6,1,4, 1, "Arktyczna Wiedza");

    @Test

    public void testCreateBohater() throws Exception {

       Bohater actual = bohater.createBohater();
       Bohater expected = new Bohater("Test Bohater", 1, 0,2,6,1,4, 1, "Arktyczna Wiedza");;

       assertEquals("Bohater nie zostal utworzony poprawnie", expected, actual);
    }
}