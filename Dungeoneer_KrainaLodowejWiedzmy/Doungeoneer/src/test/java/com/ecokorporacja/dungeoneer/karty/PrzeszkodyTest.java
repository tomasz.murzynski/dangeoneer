package com.ecokorporacja.dungeoneer.karty;


import org.junit.jupiter.api.Test;

import static org.junit.Assert.*;

public class PrzeszkodyTest {

    Mapy mapy = new Mapy("Niebezpieczne rozstaje", RodzajKarty.KARTA_PRZYGODY,
            3, 2, false, true, new Drzwi[4] ,6);



    @Test
    public void testPrzeszkody(){
        Boolean act = false;
        Boolean exp = mapy.getObstacle();
        if(exp){
            act = true;
        }
        assertTrue( "Ta karta nie zawiera przeszkody", act);
    }

    @Test
    public void testSilyPrzeszkody(){
        Boolean act = false;
        if (mapy.getSilaPrzeszkody()==6){
            act = true;
        }
        assertTrue("Sila przeszkody niezgodna",act );
    }



}