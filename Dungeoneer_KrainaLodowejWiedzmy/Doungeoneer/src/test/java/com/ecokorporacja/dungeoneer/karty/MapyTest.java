package com.ecokorporacja.dungeoneer.karty;

import org.junit.Test;


import static org.junit.jupiter.api.Assertions.assertTrue;

public class MapyTest {

    // Stworz karte mapy
    Drzwi[] drzwi = new Drzwi[4];

    Mapy kartaWejscia = new Mapy("Północna Wioska", RodzajKarty.KARTA_WEJSCIA, 1,
            1, true, drzwi);


    @Test
    public void createCardOfEntryTest() {
        Boolean actual = false;
        if (kartaWejscia.equals(new Mapy(RodzajKarty.KARTA_WEJSCIA))){
            actual = true;
        }
        assertTrue(actual, "Rodzaj karty jest niepoprawna");
    }

    @Test
    public void createCardTest() {
        Boolean actual = false;
        if (kartaWejscia.equals(new Mapy("Północna Wioska", RodzajKarty.KARTA_WEJSCIA, 1,
                1, true, drzwi))){
            actual = true;
        }
        assertTrue(actual, "Porównanie karty zakończone niepowodzeniem");
    }


}