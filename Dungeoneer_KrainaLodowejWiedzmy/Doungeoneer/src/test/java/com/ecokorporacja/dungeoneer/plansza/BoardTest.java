package com.ecokorporacja.dungeoneer.plansza;

import com.ecokorporacja.dungeoneer.karty.Mapy;
import com.ecokorporacja.dungeoneer.karty.RodzajKarty;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.*;

public class BoardTest {

    Board board = new Board(64);
    Mapy[][] setUpBoard = new Mapy[board.size][board.size];
    Mapy kartaWejscia = new Mapy(RodzajKarty.KARTA_WEJSCIA);
    Mapy kartaPrygody = new Mapy(RodzajKarty.KARTA_PRZYGODY);


    @Test
    public void createBoard() {
        Boolean actual = false;
        int size = board.size;
        if (size==64){
            actual= true;
        }
    assertTrue(actual, "Oczekiwana rozmiar planszy (64)");
    }

    @Test
    public void centerBoard(){
        int expected = 32;
        int center =  board.size / 2;
        assertEquals(expected , center, "To nie jest komorka nalezaca do centrum");
    }

    @Test
    public void setUpBoard(){
        Boolean actual = false;
        setUpBoard[board.centerBoard][board.centerBoard] = new Mapy("Karta Wejścia");
        if (setUpBoard[board.centerBoard][board.centerBoard].equals(new Mapy("Karta Wejścia"))){
                actual = true;
        }
        assertTrue(actual, "Karta wejscia jest niepoprawna");
    }

    @Test
    public void checkKindOfCards(){
        Boolean actual = false;
        if (kartaWejscia.equals(new Mapy(RodzajKarty.KARTA_WEJSCIA))){
            actual = true;
        }
        assertTrue(actual, "Rodzaj karty jest niepoprawna");
    }

    @Test
    public void checkJurnalCard(){
        Boolean actual = false;
        if (kartaPrygody.equals(new Mapy(RodzajKarty.KARTA_PRZYGODY))){
            actual = true;
        }
        assertTrue(actual, "Rodzaj karty jest niepoprawna");
    }


}