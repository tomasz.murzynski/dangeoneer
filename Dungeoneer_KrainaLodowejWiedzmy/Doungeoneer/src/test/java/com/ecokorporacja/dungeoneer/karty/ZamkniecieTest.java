package com.ecokorporacja.dungeoneer.karty;

import org.junit.jupiter.api.Test;

import static org.junit.Assert.*;

public class ZamkniecieTest {

    @Test
    public void  testStrongOFClose(){
        Drzwi drzwi = new Drzwi(true,true,true);
        Zamkniecie zamkniecie= new Zamkniecie(3);
        Boolean actual = true;
        if(drzwi.getOpen()&&drzwi.getTrap()&&drzwi.getClose()) {
            if (zamkniecie.getTrudnosc()==3){
                actual = false;
            }

        }
        assertTrue( "Zamkniecie niezgodne",!actual);
    }
}