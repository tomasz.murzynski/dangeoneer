package com.ecokorporacja.dungeoneer.karty;


import org.junit.jupiter.api.Test;


import static org.junit.Assert.*;

public class PulapkaTest {


    @Test
    public void  testFail(){
        Drzwi drzwi = new Drzwi(true,true,false);
        Pulapka pulapka = new Pulapka(2);
        Boolean actual = true;
        if(drzwi.getOpen()&&drzwi.getTrap()&&!drzwi.getClose()) {
            if (pulapka.getTrudnosc()==2){
                actual = false;
            }

        }
        assertTrue( "Pulapka niezgodna",!actual);
    }
}