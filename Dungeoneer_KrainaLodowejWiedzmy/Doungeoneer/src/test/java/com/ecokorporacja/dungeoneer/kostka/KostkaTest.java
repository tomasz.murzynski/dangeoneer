package com.ecokorporacja.dungeoneer.kostka;


import org.junit.jupiter.api.Test;


import static org.junit.jupiter.api.Assertions.assertTrue;


public class KostkaTest {


    Kostka kostka = new Kostka();


    @Test
    public void rzutKostka() {
        Boolean actual = false;
        int result = kostka.RzutKostka();
        if (result >= 1 && result <= 6) {
            actual = true;
        }

        assertTrue(actual, "Oczekiwana liczba wylosowanych oczek kostki (1-6)");
    }
}