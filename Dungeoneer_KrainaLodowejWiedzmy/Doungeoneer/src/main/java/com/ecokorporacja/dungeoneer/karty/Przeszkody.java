package com.ecokorporacja.dungeoneer.karty;

public enum Przeszkody {
    DOLY,
    OSTRZA,
    RYZYKO,
    KANAL,
    TEREN,
    PORTAL,
    WEJSCIE,
}
