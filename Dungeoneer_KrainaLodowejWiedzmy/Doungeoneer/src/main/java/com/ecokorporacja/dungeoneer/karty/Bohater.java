package com.ecokorporacja.dungeoneer.karty;

import com.ecokorporacja.dungeoneer.zetony.Chwala;

import java.util.Objects;

public class Bohater {

    String imieBohatera;
    int poziom;
    int atrybutSila;
    int atrybutMagia;
    int atrybutSzybkosc;


    int punktyZycia;
    int limitSkarbow;
    int limitDarowLosu;
    String specjalnaZdolnoscBohatera;
    int punktyChwaly;
    int punktyGrosby;

    //Punkty punkty = new Punkty(1,1);


    public Bohater(String imieBohatera, int poziom, int atrybutSila, int atrybutMagia,
                   int atrybutSzybkosc, int punktyZycia, int limitSkarbow, int limitDarowLosu,
                   String specjalnaZdolnoscBohatera, int punktyChwaly, int punktyGrosby) {
        this.imieBohatera = imieBohatera;
        this.poziom = poziom;
        this.atrybutSila = atrybutSila;
        this.atrybutMagia = atrybutMagia;
        this.atrybutSzybkosc = atrybutSzybkosc;
        this.punktyZycia = punktyZycia;
        this.limitSkarbow = limitSkarbow;
        this.limitDarowLosu = limitDarowLosu;
        this.specjalnaZdolnoscBohatera = specjalnaZdolnoscBohatera;
        this.punktyChwaly = punktyChwaly;
        this.punktyGrosby = punktyGrosby;
    }

    public Bohater(String imieBohatera, int poziom, int atrybutSila, int atrybutMagia,
                   int atrybutSzybkosc, int punktyZycia, int limitSkarbow,
                   int limitDarowLosu, String specjalnaZdolnoscBohatera) {
        this.imieBohatera = imieBohatera;
        this.poziom = poziom;
        this.atrybutSila = atrybutSila;
        this.atrybutMagia = atrybutMagia;
        this.atrybutSzybkosc = atrybutSzybkosc;
        this.punktyZycia = punktyZycia;
        this.limitSkarbow = limitSkarbow;
        this.limitDarowLosu = limitDarowLosu;
        this.specjalnaZdolnoscBohatera = specjalnaZdolnoscBohatera;
        this.punktyChwaly = 1;
        this.punktyGrosby = 1;
    }



    public Bohater createBohater(){
        Bohater gracz1 = new Bohater("Test Bohater", 1, 0,
                2,6,1,
                4, 1, "Arktyczna Wiedza", 1,1);
        return  gracz1;
    }

    public int getPunktyChwaly() {
        return punktyChwaly;
    }

    public void setPunktyChwaly(int punktyChwaly) {
        this.punktyChwaly = punktyChwaly;
    }

    public String getImieBohatera() {
        return imieBohatera;
    }

    public void setImieBohatera(String imieBohatera) {
        this.imieBohatera = imieBohatera;
    }

    public int getPoziom() {
        return poziom;
    }

    public void setPoziom(int poziom) {
        this.poziom = poziom;
    }

    public int getAtrybutSila() {
        return atrybutSila;
    }

    public void setAtrybutSila(int atrybutSila) {
        this.atrybutSila = atrybutSila;
    }

    public int getAtrybutMagia() {
        return atrybutMagia;
    }

    public void setAtrybutMagia(int atrybutMagia) {
        this.atrybutMagia = atrybutMagia;
    }

    public int getAtrybutSzybkosc() {
        return atrybutSzybkosc;
    }

    public void setAtrybutSzybkosc(int atrybutSzybkosc) {
        this.atrybutSzybkosc = atrybutSzybkosc;
    }

    public int getPunktyZycia() {
        return punktyZycia;
    }

    public void setPunktyZycia(int punktyZycia) {
        this.punktyZycia = punktyZycia;
    }

    public int getLimitSkarbow() {
        return limitSkarbow;
    }

    public void setLimitSkarbow(int limitSkarbow) {
        this.limitSkarbow = limitSkarbow;
    }

    public int getLimitDarowLosu() {
        return limitDarowLosu;
    }

    public void setLimitDarowLosu(int limitDarowLosu) {
        this.limitDarowLosu = limitDarowLosu;
    }

    public String getSpecjalnaZdolnoscBohatera() {
        return specjalnaZdolnoscBohatera;
    }

    public void setSpecjalnaZdolnoscBohatera(String specjalnaZdolnoscBohatera) {
        this.specjalnaZdolnoscBohatera = specjalnaZdolnoscBohatera;
    }

    @Override
    public String toString() {
        return "Bohater{" +
                "imieBohatera='" + imieBohatera + '\'' +
                ", poziom=" + poziom +
                ", atrybutSila=" + atrybutSila +
                ", atrybutMagia=" + atrybutMagia +
                ", atrybutSzybkosc=" + atrybutSzybkosc +
                ", punktyZycia=" + punktyZycia +
                ", limitSkarbow=" + limitSkarbow +
                ", limitDarowLosu=" + limitDarowLosu +
                ", specjalnaZdolnoscBohatera='" + specjalnaZdolnoscBohatera + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Bohater)) return false;
        Bohater bohater = (Bohater) o;
        return getPoziom() == bohater.getPoziom() &&
                getAtrybutSila() == bohater.getAtrybutSila() &&
                getAtrybutMagia() == bohater.getAtrybutMagia() &&
                getAtrybutSzybkosc() == bohater.getAtrybutSzybkosc() &&
                getPunktyZycia() == bohater.getPunktyZycia() &&
                getLimitSkarbow() == bohater.getLimitSkarbow() &&
                getLimitDarowLosu() == bohater.getLimitDarowLosu() &&
                Objects.equals(getImieBohatera(), bohater.getImieBohatera()) &&
                Objects.equals(getSpecjalnaZdolnoscBohatera(), bohater.getSpecjalnaZdolnoscBohatera());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getImieBohatera(), getPoziom(), getAtrybutSila(), getAtrybutMagia(), getAtrybutSzybkosc(), getPunktyZycia(), getLimitSkarbow(), getLimitDarowLosu(), getSpecjalnaZdolnoscBohatera());
    }
}
