package com.ecokorporacja.dungeoneer.karty;

public class Zamkniecie {
    int silaZamkniecia;


    public Zamkniecie(int silaZamkniecia) {
        this.silaZamkniecia = silaZamkniecia;
    }

    public int getTrudnosc() {
        return silaZamkniecia;
    }

    public void setTrudnosc(int silaZamkniecia) {
        this.silaZamkniecia = silaZamkniecia;
    }
}

