package com.ecokorporacja.dungeoneer.karty;

import java.util.Objects;

public class Mapy {

    String nazwaKarty;


    RodzajKarty rodzajKarty;
    int punktyGrosby, punktyChwaly;
    Boolean isSpecjalnyEfekt;
    Boolean isObstacle; //Przeszkody
    Drzwi[] drzwi = new Drzwi[4];
    int silaPrzeszkody;



    public Mapy(String nazwaKarty, RodzajKarty rodzajKarty, int punktyGrosby, int punktyChwaly,
                Boolean isSpecjalnyEfekt, Boolean isObstacle, Drzwi[] drzwi, int silaPrzeszkody) {
        this.nazwaKarty = nazwaKarty;
        this.rodzajKarty = rodzajKarty;
        this.punktyGrosby = punktyGrosby;
        this.punktyChwaly = punktyChwaly;
        this.isSpecjalnyEfekt = isSpecjalnyEfekt;
        this.isObstacle = isObstacle;
        this.drzwi = drzwi;
        this.silaPrzeszkody = silaPrzeszkody;
    }

    public Mapy(String nazwaKarty, RodzajKarty rodzajKarty, int punktyGrosby, int punktyChwaly,
                Boolean isSpecjalnyEfekt, Drzwi[] drzwi) {
        this.nazwaKarty = nazwaKarty;
        this.rodzajKarty = rodzajKarty;
        this.punktyGrosby = punktyGrosby;
        this.punktyChwaly = punktyChwaly;
        this.isSpecjalnyEfekt = isSpecjalnyEfekt;
        this.drzwi = drzwi;
        this.isObstacle = false;
    }

    public Mapy(String nazwaKarty) {
        this.nazwaKarty = nazwaKarty;
    }

    public Mapy(RodzajKarty rodzajKarty) {
        this.rodzajKarty = rodzajKarty;
        if (rodzajKarty.equals(new Mapy(RodzajKarty.KARTA_WEJSCIA))){

            Drzwi[] drzwi = new Drzwi[4];
            drzwi[0] = new Drzwi(true, false);
            drzwi[1] = new Drzwi(true, false);
            drzwi[2] = new Drzwi(true, false);
            drzwi[3] = new Drzwi(true, false);

            this.nazwaKarty = "Północna Wioska";
            this.punktyGrosby = 1;
            this.punktyChwaly = 1;
            this.isSpecjalnyEfekt = true;
            this.isObstacle = false;
            this.drzwi = drzwi;
            this.silaPrzeszkody = 0;
        }
    }

    public String getNazwaKarty() {
        return nazwaKarty;
    }

    public void setNazwaKarty(String nazwaKarty) {
        this.nazwaKarty = nazwaKarty;
    }

    public Boolean getObstacle() {
        return isObstacle;
    }

    public void setObstacle(Boolean obstacle) {
        isObstacle = obstacle;
    }

    public int getSilaPrzeszkody() {
        return silaPrzeszkody;
    }

    public void setSilaPrzeszkody(int silaPrzeszkody) {
        this.silaPrzeszkody = silaPrzeszkody;
    }

    public RodzajKarty getRodzajKarty() {
        return rodzajKarty;
    }

    public void setRodzajKarty(RodzajKarty rodzajKarty) {
        this.rodzajKarty = rodzajKarty;
    }

    public int getPunktyGrosby() {
        return punktyGrosby;
    }

    public void setPunktyGrosby(int punktyGrosby) {
        this.punktyGrosby = punktyGrosby;
    }

    public int getPunktyChwaly() {
        return punktyChwaly;
    }

    public void setPunktyChwaly(int punktyChwaly) {
        this.punktyChwaly = punktyChwaly;
    }

    public Boolean getSpecjalnyEfekt() {
        return isSpecjalnyEfekt;
    }

    public void setSpecjalnyEfekt(Boolean specjalnyEfekt) {
        isSpecjalnyEfekt = specjalnyEfekt;
    }

    public Drzwi[] getDrzwi() {
        return drzwi;
    }

    public void setDrzwi(Drzwi[] drzwi) {
        this.drzwi = drzwi;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Mapy)) return false;
        Mapy mapy = (Mapy) o;
        return rodzajKarty == mapy.rodzajKarty;
    }

    @Override
    public int hashCode() {
        return Objects.hash(rodzajKarty);
    }

    public void createCardOfEntry(Bohater bohater) {

        // Stworz karte mapy
        Drzwi[] drzwi = new Drzwi[4];
        drzwi[0] = new Drzwi(true, false);
        drzwi[1] = new Drzwi(true, false);
        drzwi[2] = new Drzwi(true, false);
        drzwi[3] = new Drzwi(true, false);
        Mapy kartaMapy = new Mapy("Północna Wioska", RodzajKarty.KARTA_WEJSCIA, 1,
                1, true, drzwi);
        if (kartaMapy.getSpecjalnyEfekt() == true) {
            //Odrzucenie możesz odrzucic 2 punkty chwały by odrzucic lub wylosowac 1 nowe heroiczne zadanie
            if (bohater.getPunktyChwaly()>=2){
                System.out.println("Wymiana punktów chwały jest możliwa");
                //Umożliwij wymiane heroicznego zadania
            }else System.out.println("Wymiana punktów chwały jest niemożliwa");
            //Nie można rzucac wyzwań
        }
    }


}
