package com.ecokorporacja.dungeoneer.karty;


import com.ecokorporacja.dungeoneer.generatory.drzwi.DoorGenerator;

import java.util.Random;

public class Drzwi implements DoorGenerator {
    Boolean isOpen;
    Boolean isTrap;
    Boolean isClose;
    Random random = new Random();
    int silaPulapki;
    int silaZamkniecia;

    public Drzwi(int silaPulapki, int silaZamkniecia) {
        this.silaPulapki = silaPulapki;
        this.silaZamkniecia = silaZamkniecia;
    }

    public Drzwi(Boolean isOpen, Boolean isTrap) {
        this.isOpen = isOpen;
        this.isTrap = isTrap;
    }

    public Drzwi(Boolean isOpen, Boolean isTrap, Boolean isClose) {
        this.isOpen = isOpen;
        this.isTrap = isTrap;
        this.isClose = isClose;
    }

    public Boolean getOpen() {
        return isOpen;
    }

    public void setOpen(Boolean open) {
        isOpen = open;
    }

    public Boolean getTrap() {
        return isTrap;
    }

    public void setTrap(Boolean trap) {
        isTrap = trap;
    }

    public Boolean getClose() {
        return isClose;
    }

    public void setClose(Boolean close) {
        isClose = close;
    }

    public int getSilaPulapki() {
        return silaPulapki;
    }

    public void setSilaPulapki(int silaPulapki) {
        this.silaPulapki = silaPulapki;
    }

    public int getSilaZamkniecia() {
        return silaZamkniecia;
    }

    public void setSilaZamkniecia(int silaZamkniecia) {
        this.silaZamkniecia = silaZamkniecia;
    }

    //Door generator
    void createDoor() {

        Drzwi drzwi = new Drzwi(random.nextBoolean(), random.nextBoolean(),false);
        if (drzwi.getOpen()) {
            System.out.println("Door open");
            if (drzwi.getTrap()) {
                //Random trap
                System.out.println("CheckPoint it's trap");
            } else {
                //Random close
                System.out.println("CheckPoint isn't trap");
            }
        } else System.out.println("Is wall");
    }

    @Override
    public Drzwi[] DoorGenerator() {
        Drzwi[] door = new Drzwi[4];

        door[0]= new Drzwi(random.nextBoolean(), random.nextBoolean(),random.nextBoolean());
        door[1]= new Drzwi(random.nextBoolean(), random.nextBoolean(),random.nextBoolean());
        door[2]= new Drzwi(random.nextBoolean(), random.nextBoolean(),random.nextBoolean());
        door[3]= new Drzwi(random.nextBoolean(), random.nextBoolean(),random.nextBoolean());

        for (int i = 0; i < door.length; i++) {
            if (door[i].getOpen()==true&&door[i].getTrap()==true&&door[i].getClose()==true){
                //Stworz silaZamkniecia
                door[i].setSilaZamkniecia(random.nextInt(6));
            }else if(door[i].getOpen()==true&&door[i].getTrap()==true&&door[i].getClose()==false){
                //Stworz silePulapki
                door[i].setSilaPulapki(random.nextInt(6));
            }else {
                door[i].setSilaZamkniecia(0);
                door[i].setSilaPulapki(0);
            }
        }


        return door ;
    }
}
