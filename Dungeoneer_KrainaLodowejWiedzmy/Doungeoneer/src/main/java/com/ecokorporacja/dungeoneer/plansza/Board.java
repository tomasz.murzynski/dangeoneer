package com.ecokorporacja.dungeoneer.plansza;

import com.ecokorporacja.dungeoneer.generatory.drzwi.DoorGenerator;
import com.ecokorporacja.dungeoneer.generatory.plansza.BoardGenerator;
import com.ecokorporacja.dungeoneer.karty.Drzwi;
import com.ecokorporacja.dungeoneer.karty.Mapy;
import com.ecokorporacja.dungeoneer.karty.RodzajKarty;

public class Board implements BoardGenerator {


    public int size; //
    int centerBoard = size/2;

    public Board(){ this.size=8; }

    public Board(int size){
        this.size = size;

    }

//    public Mapy[][] CreateBoard(){
//        Mapy[][] board = new Mapy[size][size];
//        return board;
//    }

    void SetUpBoard(){



            //Wysnaczyć środek planszy i wstaw tam karte wejscia.

            //Umieścić karte wejścia na środku planszy

            //Do okoła karty wejścia umieścić 4 losowe katry mapy zgodnie z regułami gry

            //Testy
       
    }


    @Override
    public Mapy[][] BoardSetup() {
        Mapy[][] board = new Mapy[size][size];
        board[centerBoard][centerBoard] = new Mapy(RodzajKarty.KARTA_WEJSCIA);
        System.out.println(board[centerBoard][centerBoard]+"  " + "cos") ;
        DoorGenerator dg = new DoorGenerator() {
            @Override
            public Drzwi[] DoorGenerator() {
                return new Drzwi[0];
            }
        };

        board[centerBoard][centerBoard-1] = new Mapy("Niebezpieczne Rozstaje",RodzajKarty.KARTA_PRZYGODY,3,2,false,
                true, dg.DoorGenerator(),6);
        board[centerBoard+1][centerBoard] = new Mapy("Zakret",RodzajKarty.KARTA_PRZYGODY,2,
                1,false,
                false, dg.DoorGenerator(),0);
        board[centerBoard][centerBoard+1] = new Mapy("Rozdoza",RodzajKarty.KARTA_PRZYGODY,2,1,false,
                true, dg.DoorGenerator(),0);
        board[centerBoard-1][centerBoard] = new Mapy("Rozwidlenie",RodzajKarty.KARTA_PRZYGODY,2,1,false,
                false, dg.DoorGenerator(),0);
        System.out.println("generator planszy :)");
        return board;
    }
}
