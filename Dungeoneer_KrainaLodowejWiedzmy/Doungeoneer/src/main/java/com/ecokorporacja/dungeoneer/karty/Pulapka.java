package com.ecokorporacja.dungeoneer.karty;

public class Pulapka {
    int silaPulapki;

    public Pulapka(int silaPulapki) {
        this.silaPulapki = silaPulapki;
    }

    public int getTrudnosc() {
        return silaPulapki;
    }

    public void setTrudnosc(int silaPulapki) {
        this.silaPulapki = silaPulapki;
    }
}
