package com.ecokorporacja.dungeoneer;

import com.ecokorporacja.dungeoneer.generatory.plansza.BoardGenerator;
import com.ecokorporacja.dungeoneer.karty.Bohater;
import com.ecokorporacja.dungeoneer.karty.Drzwi;
import com.ecokorporacja.dungeoneer.karty.Mapy;
import com.ecokorporacja.dungeoneer.karty.RodzajKarty;
import com.ecokorporacja.dungeoneer.kostka.Kostka;
import com.ecokorporacja.dungeoneer.plansza.Board;
import jdk.nashorn.internal.runtime.Debug;
import sun.rmi.runtime.Log;

import java.io.Console;
import java.util.Random;


public class Main {

    public static void main(String[] args) {

        // Wykonaj 100 rzutów kostką 6 ścienną. Wynik wypisz w rzędach po 10 rzutów.
        Kostka kostka = new Kostka();

        for (int i = 0; i < 100; i++) {
            if (i % 10 == 0) {
                System.out.println();
            }
            System.out.print(kostka.RzutKostka() + " ");
        }
        System.out.println("");


        // Stwórz postać gracza

        Bohater gracz1 = new Bohater("Tomasz", 1, 0,
                2, 6, 1, 4,
                1, "Arktyczna Wiedza", 1,1);

        BoardGenerator bg = new BoardGenerator() {

            @Override
            public Mapy[][] BoardSetup() {
                return new Mapy[64][64];
            }
        };
        System.out.println(bg.toString());
        System.out.println(bg.BoardSetup());


    }






}
