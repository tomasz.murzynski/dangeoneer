package com.ecokorporacja.dungeoneer.generatory.plansza;

import com.ecokorporacja.dungeoneer.karty.Mapy;
import com.ecokorporacja.dungeoneer.plansza.Board;

public interface BoardGenerator {
    Mapy[][] BoardSetup();
}
