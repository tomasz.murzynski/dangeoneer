package com.ecokorporacja.dungeoneer.zetony;

public class Chwala {
    int punkty;

    public Chwala(int punkty) {
        this.punkty = punkty;
    }

    public int getPunkty() {
        return punkty;
    }

    public void setPunkty(int punkty) {
        this.punkty = punkty;
    }
}
